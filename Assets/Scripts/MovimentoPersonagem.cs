﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimentoPersonagem : MonoBehaviour {

	public float speed = 4.0f;
	public int xp = 0;

	private Vector3 pos, pos2;
	private Transform tr;
	private bool isMoving, roadCollide = false;
	private float multiplicador = 0.1f;

	private int quantidadeItensNecessaria;

	void Start () {
		Debug.Log ("Starting");

		pos = transform.position;
		pos2 = pos; 
		tr = transform;

		quantidadeItensNecessaria = GameObject.FindGameObjectsWithTag("ItemColeta").Length;

	}

	void Update () {

		if (!isMoving) {
			
			pos2 = transform.position;

			if (tr.position == pos) {
				//direita
				if(Input.GetKey(KeyCode.D))
					pos += (Vector3.right * multiplicador);

				//esquerda
				else if(Input.GetKey(KeyCode.A))
					pos += (Vector3.left * multiplicador);

				//para cima
				else if(Input.GetKey(KeyCode.W))
					pos += (Vector3.up * multiplicador);

				//para baixo
				else if(Input.GetKey(KeyCode.S))
					pos += (Vector3.down * multiplicador);
			}

			float step = Time.deltaTime * speed;
			transform.position = Vector3.MoveTowards (transform.position, pos, step);
			isMoving = false;

			if (!roadCollide) {
				transform.position = pos2;
				pos = pos2;
				roadCollide = true;
			}

		}

	}

	void OnCollisionEnter2D (Collision2D other) {
		
		if (other.gameObject.tag == "ItemColeta") {
			Destroy (other.gameObject);
			xp++;

			CheckVictory ();

		} else {
			roadCollide = false;
		}
	}

	void OnCollisionStay2D (Collision2D other) {
		
		if (other.gameObject.tag != "ItemColeta") {
			roadCollide = false;
		}
	}

	private void CheckVictory(){

		Debug.Log ("Checando vitória. Quantidade atual de xp: " + xp);

		if (xp == quantidadeItensNecessaria) {
			//todo os itens pegos
			//carregando cena seguinte
			Debug.Log ("Vitória!");
			Application.LoadLevel ("game_over_scene");
		}
	}

}
